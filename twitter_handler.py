#! /usr/bin/env python3
"""
twitter_handler.py
author: Jonathan Lacson

Simple interface for Tweepy
"""
# builtin imports
import os

# import Tweepy
import tweepy

class TweepyInterface(object):
    """
    Main class to handle interfacing with Twitter.
    """

    # ---------------------
    #   PRIVATE METHODS   |
    # ---------------------
    @staticmethod
    def _getCredentials():
        """
        Gets credentials to sign into Twitter.
        Checks for credential file (local) or pulls from Heroku environment vars

        :return: Consumer Key, Consumer Secret, Access Key, and Access Secret (or false if unsuccessful)
        """
        # attempt importing from file
        if os.path.exists("twitter_credentials.py") is True:
            try:
                from twitter_credentials import CONSUMER_KEY, \
                                                CONSUMER_SECRET, \
                                                ACCESS_KEY, \
                                                ACCESS_SECRET
            except ImportError as e:
                print(e)
                return None

        # if the file doesn't exist, we're probably in Heroku
        else:
            try:
                CONSUMER_KEY    = os.environ['CONSUMER_KEY']
                CONSUMER_SECRET = os.environ['CONSUMER_SECRET']
                ACCESS_KEY      = os.environ['ACCESS_KEY']
                ACCESS_SECRET   = os.environ['ACCESS_SECRET']
            except KeyError as e:
                print(e)
                return None

        # if we're here, everything worked, return everything
        return CONSUMER_KEY, CONSUMER_SECRET, ACCESS_KEY, ACCESS_SECRET

    # -----------------
    #   CONSTRUCTOR   |
    # -----------------
    def __init__(self):
        """
        Constructor.
        Throws exception if credentials could not be found.
        """
        # get credentials
        creds = self._getCredentials()

        # check if we actually got credentials
        if not creds:
            raise EnvironmentError("Could not get credentials.")

        # instantiate tweepy
        tweepyAuth = tweepy.OAuthHandler(creds[0], creds[1])
        tweepyAuth.set_access_token(creds[2], creds[3])
        self.api = tweepy.API(tweepyAuth)

        # check if credentials are valid
        if not self.api.verify_credentials():
            raise EnvironmentError("Credentials are not valid.")

    # --------------------
    #   PUBLIC METHODS   |
    # --------------------
    def tweet(self, text=None):
        """
        Method to tweet.
        
        :return: True if successful, False if not.
        """
        # throw an exception if the tweet is too long
        if len(text) > 280:
            raise SyntaxError("Tweet is too long.")

        try:
            self.api.update_status(text)
        except Exception as e:  # TODO: use a less broad exception
            print(e)
            return False
        else:
            return True

    def getTweets(self):
        """
        Gets the latest tweets.

        :return: True if successful, False if not.
        """
        pass  # TODO: write me
