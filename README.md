# He Needs Some Vines

[(*Relevant Reference*)](https://www.youtube.com/watch?v=QDPGUH2JRUE)

Replies to specific tweets with relevant vines.

Language: Python 3

## Technologies Used

The following frameworks and Python packages are used:

* [`tweepy`](https://github.com/tweepy/tweepy/) to communicate with Twitter
* [`num2words`](https://github.com/savoirfairelinux/num2words) to convert numbers in tweets to words
* [`gensim`](https://radimrehurek.com/gensim/index.html) to generate sentence similarities
    - We are using the [TF-IDF (Term Frequency - Inverse Document Frequency) Model](https://radimrehurek.com/gensim/models/tfidfmodel.html)
    to calculate similarity values between sentences. The LSI model referenced in the tutorial was not accurate enough.

## Getting Started

### Updating Vine Database

To add custom entries to search, add them to the `vines` list in `data/vinelist.py`.
Then from terminal, run `python3 process_vinelist.py` to regenerate the dictionary, corpus, and index files.

### Getting Similarity Values

`test_similarity.py` can be used to test the similarity queries.

```python
>>> from test_similarity import SimilarityTester
>>> s = SimilarityTester()
```

* To view the top three results, use the `query()` function:

```python
>>> s.query("your mom's a hoe")
Results for 'your mom's a hoe':
#1. Score: 0.6386439
    Whoever threw that paper your moms a hoe
    https://vine.co/v/ODDUaQWOJJt
#2. Score: 0.091254935
    dude what the fuck this is your space this is your area they cant do that here
    https://vine.co/v/OmXXFDaQrDE
#3. Score: 0.08422181
    so i'm just standing there barbecue sauce on my titties
    https://www.youtube.com/watch?v=Wo9p4Lqaakg
>>>
```

* To return the link and the similarity score of a string, use the `getClosestMatch()` function:

```python
>>> link, score = s.getClosestMatch("you stupid")
>>> link
'https://vine.co/v/huwVJIEJW50'
>>> score
0.42249304
>>>
```

## Attribution

The following sources were used as guidance in the creation of this bot:

### Software Tutorials
* [How to Set up a Twitter Bot with Python and Heroku](https://dev.to/emcain/how-to-set-up-a-twitter-bot-with-python-and-heroku-1n39)
* [GenSim Tutorials](https://radimrehurek.com/gensim/tutorial.html)

### Technical Papers / Discussion
* [How can I create my own stopwords list on the basis of my text?](https://www.quora.com/How-can-I-create-my-own-stopwords-list-on-the-basis-of-my-text)
* [Stack Overflow - Similarity between two text documents](https://stackoverflow.com/questions/8897593/similarity-between-two-text-documents)

### Misc. Resources (Vine Lists)
* [The Top 100 Vines of All Time](https://www.theodysseyonline.com/vine-thread)
* [random vine playlist because rip vine](https://www.youtube.com/playlist?list=PL9dH-fpu0-eee-c6InNvXBkp7E0es2yVy)s