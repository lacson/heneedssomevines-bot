#! /usr/bin/env python3
"""
process_vinelist.py
author: Jonathan Lacson

Takes the list of tuples in vinelist.py and converts
relevant strings to vectors to become a corpus (for similarity lookup)
"""
from gensim import corpora, models, similarities
from collections import defaultdict
from vinelist import vines
from stoplist import stoplist

# import our data
vinedescs = []
for vinetuple in vines:
    vinedescs.append(vinetuple[1].replace("'", ""))

# remove common words and tokenize data
tokenizedDescs = [[word for word in desc.lower().split() if word not in stoplist] \
                   for desc in vinedescs ]

# count frequency of words
wordFrequency = defaultdict(int)
for desc in tokenizedDescs:
    for token in desc:
        wordFrequency[token] += 1

'''
# clear out words that only show up once
tokenizedDescs = [[token for token in desc if wordFrequency[token] > 1] \
                  for desc in tokenizedDescs]
'''

# save our tokenized list to a dictionary
DICTIONARY_NAME='vines.dict'
descDict = corpora.Dictionary(tokenizedDescs)
try:
    descDict.save(DICTIONARY_NAME)
except Exception as e:
    print('Error saving tokenizedDescs to %s: %s' % (DICTIONARY_NAME, e))
else:
    print('Successfully saved tokenizedDescs to %s' % DICTIONARY_NAME)

# create corpus file
CORPUS_NAME='vines.mm'
corpus = [descDict.doc2bow(desc) for desc in tokenizedDescs]
try:
    corpora.MmCorpus.serialize(CORPUS_NAME, corpus)
except Exception as e:
    print('Error saving corpus file to disk to %s: %s' % (CORPUS_NAME, e))
else:
    print('Successfully saved corpus file to %s' % CORPUS_NAME)

# create Tf-Idf model
tfidf = models.TfidfModel(corpus, id2word=descDict)

# create index
index = similarities.MatrixSimilarity(tfidf[corpus], num_features=len(descDict))

# save index
INDEX_NAME='vines.index'
try:
    index.save(INDEX_NAME)
except Exception as e:
    print('Error saving index file to disk to %s: %s' % (INDEX_NAME, e))
else:
    print('Successfully saved index file to %s' % INDEX_NAME)
