#! /usr/bin/env python3
"""
test_similarity.py
author: Jonathan Lacson

Takes generated dictionary and corpus files and allows
user to input string to test similarity feature.
"""
import os
import string
import code

from importlib import import_module
from gensim import corpora, models, similarities
from num2words import num2words

# ---------------
#   CONSTANTS   |
# ---------------
DICT_FILE   = 'data/vines.dict'
CORPUS_FILE = 'data/vines.mm'
INDEX_FILE  = 'data/vines.index'
LIST_FILE   = 'data/vinelist.py'
STOPLIST_FILE = 'data/stoplist.py'

NEEDED_FILES = [ DICT_FILE,
                 CORPUS_FILE,
                 INDEX_FILE,
                 LIST_FILE  ]

def isInteger(num):
    """
    Checks if a string is really an integer.

    :param num: String to test.
    :return: True if true, False if not.
    """
    try:
        int(num)
        return True
    except ValueError:
        return False

class SimilarityTester(object):
    """
    Main class to interface with gensim dictionary
    and corpus files.
    """

    # ---------------------
    #   PRIVATE METHODS   |
    # ---------------------
    def _tokenizeString(self, text):
        """
        Takes a string, strips punctuation and tokenizes.

        :return: tokenized string (type list of str)
        """
        # don't overwrite original input
        procText = text

        # remove punctuation
        translator = str.maketrans('', '', string.punctuation)
        procText = procText.translate(translator)
        
        # remove common words and tokenize
        procText = [word for word in procText.lower().split() if word not in self.stoplist]

        # convert numbers to words
        for idx, val in enumerate(procText):
            if isInteger(val):
                procText[idx] = num2words(int(val))

        return procText

    def _getSimilarities(self, vecText):
        """
        Checks text to return similar vines.

        :param vecText: split text (type list of str)
        :return: sorted list of similar vines (type list of tup(int, float))
        """
        # vectorize our text
        vecText = self.dic.doc2bow(vecText)

        # convert text to TF-Idf space
        vecTFIDF = self.tfidf[vecText]

        # compare text against indexed documents
        similar = self.index[vecTFIDF]

        # sort by score
        similar = sorted(enumerate(similar), key=lambda item: -item[1])

        return similar

    def _loadVineList(self):
        """
        Pulls list of vines from list file.

        :return: True if successful, False if not.
        """
        # some clever string manip to convert file path to module path
        modpath = LIST_FILE.replace('.py', '')
        modpath = '.'.join(modpath.split('/'))

        try:
            v = import_module(modpath)
        except ImportError as e:
            print(e)
            return False
        else:
            self.vines = v.vines
            return True

    def _loadStopList(self):
        """
        Pulls stop list from list file.

        :return: True if successful, False if not.
        """
        modpath = STOPLIST_FILE.replace('.py', '')
        modpath = '.'.join(modpath.split('/'))

        try:
            s = import_module(modpath)
        except ImportError as e:
            print(e)
            return False
        else:
            self.stoplist = s.stoplist
            return True

    def _loadGensimFiles(self):
        """
        Loads dictionary, corpus, and index files.

        :return: True if successful, False if not.
        """
        try:
            dictionary = corpora.Dictionary.load(DICT_FILE)
            corpus = corpora.MmCorpus(CORPUS_FILE)
            tfidf = models.TfidfModel(corpus, id2word=dictionary)
            index = similarities.MatrixSimilarity.load(INDEX_FILE)
        except Exception as e:
            print(e)
            return False
        else:
            self.dic = dictionary
            self.corpus = corpus
            self.tfidf = tfidf
            self.index = index
            return True

    # -----------------
    #   CONSTRUCTOR   |
    # -----------------
    def __init__(self):
        """
        Main constructor for class.
        """
        # check to see if our files exist
        if any(not os.path.exists(filepath) for filepath in NEEDED_FILES):
            print("Error, could not find all required files:")

            for files in NEEDED_FILES:
                if os.path.exists(files) is False:
                    print("-> %s was NOT FOUND" % files)

            raise RuntimeError("Missing required files.")

        # import vine list
        if self._loadVineList() is False:
            raise RuntimeError("Could not import list of vines.")

        # import stoplist
        if self._loadStopList() is False:
            raise RuntimeError("Could not import word stoplist.")

        # import GenSim files
        if self._loadGensimFiles() is False:
            raise RuntimeError("Could not load necessary files for similarity analysis.")

    # ----------------
    #   DESTRUCTOR   |
    # ----------------
    def __del__(self):
        """
        Destructor. Saves index.
        """
        print("Saving index file to %s" % INDEX_FILE)
        self.index.save(INDEX_FILE)

    # --------------------
    #   PUBLIC METHODS   |
    # --------------------
    def query(self, text, full=False):
        """
        Checks text against database and prints top three matches.

        :param full: whether or not to return the full sorted list (type Bool)
        :return: sorted list of similar vines (type list of tup(int, float)) if full is True
        """
        # tokenize string
        procText = self._tokenizeString(text)

        # process through TF-Idf modeling
        sims = self._getSimilarities(procText)

        print("Results for '%s':" % text)
        for i in range(3):
            vineIndex = sims[i][0]
            print("#%s. Score: %s" % (i+1, sims[i][1])) # rank and score
            print("    %s" % self.vines[vineIndex][1])  # description
            print("    %s" % self.vines[vineIndex][0])  # link

        if full:
            return sims
        
        return None

    def getClosestMatch(self, text):
        """
        Returns the first match (most similar) for a given string.

        :param text: text to lookup (type str)
        :return: link to closest match and similarity score (types str, float)
        """
        # tokenize string
        procText = self._tokenizeString(text)

        # process through TF-Idf modeling
        topMatch = self._getSimilarities(procText)[0]  # returns tuple (index, score)

        return self.vines[topMatch[0]][0], topMatch[1]

if __name__ == "__main__":
    print("test_similarity.py")
    # drop into shell
    code.interact(local=locals())
